import os
import tensorflow as tf

pathNow    = os.path.abspath(os.path.join(os.path.dirname( __file__ )))
print(pathNow)

windowSize     = 30
varName        = str(windowSize) + 'x' + str(windowSize) #'10_20x20'
print('Dataset ' + varName)

h5File         = pathNow + '/Datasets/selected_XX.h5'
fileNameReport = pathNow + '/Reports/UNet_XX.mat'
fileNameModel  = pathNow + '/Reports/Model/UNetHybrid_selected_XX.hdf5'
fileNameHist   = pathNow + '/Reports/History/UNetHybrid_selected_XX.csv'

h5File         = h5File.replace('XX', varName)
fileNameReport = fileNameReport.replace('XX', varName)
fileNameModel  = fileNameModel.replace('XX', varName)
fileNameHist   = fileNameHist.replace('XX', varName)

numClass       = 7
numEpochs      = 100
batch_size     = 128  #32
trainRatio     = 0.70
valRatio       = 0.15
testRatio      = 0.15

"""
## Prepare U-Net Xception-style model
"""
# import tensorflow as tf
# from tensorflow import kerasy
from tensorflow.keras import layers
from tensorflow.keras.utils import plot_model
from tensorflow.keras.layers import Flatten, Dense, Dropout

def getUNetModel(windowSize, num_classes):
    inputs = keras.Input((windowSize, windowSize, 8, 1))
    x = inputs
    previous_block_activation = x  # Set aside residual

    # Blocks 1, 2, 3 are identical apart from the feature depth.
    for filters in [8, 16, 32]:  # [8, 16, 32]
        x = layers.Conv3D(filters=filters, kernel_size=(3, 3, 3), activation='relu', padding="same")(x)
        x = layers.BatchNormalization()(x)
        x = layers.Conv3D(filters=filters, kernel_size=(3, 3, 3), activation='relu', padding="same")(x)
        x = layers.BatchNormalization()(x)
        x = layers.MaxPool3D(3, strides=2, padding="same")(x)

        # Project residual
        residual = layers.Conv3D(filters, 1, strides=2, padding="same")(previous_block_activation)
        x = layers.add([x, residual])  # Add back residual
        previous_block_activation = x  # Set aside next residual

    ### [Second half of the network: upsampling inputs] ###
    xShape = x.shape
    x = layers.Reshape((xShape[1], xShape[2], xShape[3]*xShape[4]))(x)
    x = layers.Conv2D(filters=64, kernel_size=(3,3), activation='relu')(x) # 64

    prevShape = previous_block_activation.shape
    previous_block_activation= layers.Reshape((prevShape[1], prevShape[2], prevShape[3]*prevShape[4]))(previous_block_activation)
    previous_block_activation = layers.Conv2D(filters=64, kernel_size=(3,3), activation='relu')(previous_block_activation) # 64

    for filters in [32, 16, 8]:  # [32, 16, 8]
        x = layers.Conv2DTranspose(filters=filters, kernel_size=3, activation='relu', padding="same")(x)
        x = layers.BatchNormalization()(x)
        x = layers.Conv2DTranspose(filters=filters, kernel_size=3, activation='relu', padding="same")(x)
        x = layers.BatchNormalization()(x)
        x = layers.UpSampling2D(2)(x)

        # Project residual
        residual = layers.UpSampling2D(2)(previous_block_activation)
        residual = layers.Conv2D(filters, 1, padding="same")(residual)
        x = layers.add([x, residual])  # Add back residual
        previous_block_activation = x  # Set aside next residual

    # Add a per-pixel classification layer
    flatten_layer = Flatten()(x)

    ## fully connected layers
    dense_layer1 = Dense(units=64, activation='relu')(flatten_layer)
    dense_layer1 = Dropout(0.4)(dense_layer1)
    dense_layer2 = Dense(units=16, activation='relu')(dense_layer1)
    dense_layer2 = Dropout(0.4)(dense_layer2)
    # outputs = Dense(units=num_classes, activation='softmax')(dense_layer2)
    outputs = Dense(units=num_classes)(dense_layer2)

    # Define the model
    model  = keras.Model(inputs=inputs, outputs=outputs)
    config = model.get_config()
    model  = keras.Model.from_config(config)
    return model

# -----------------------------------------------------------------------
# Main
from tensorflow import keras
# Free up RAM in case the model definition cells were run multiple times
keras.backend.clear_session()

from myUtils import loadDatasets
X,Y   = loadDatasets(h5File)

print("Number of samples:" + str(X.shape[0]))
print("Data Dimension X:" + str(X.shape) + ", Y:" + str(Y.shape))

from myUtils import splitTrainValTest
x_train, x_val, x_test, y_train, y_val, y_test = splitTrainValTest(X, Y, trainRatio=trainRatio, valRatio=valRatio, testRatio=testRatio)

# Build model
# model = getUNetModel(windowSize, numClass)
model = getUNetModel(windowSize, num_classes=numClass)
model.summary()
# plot_model(model, show_shapes=True)

# compiling the model
# Configure the model for training.

from tensorflow.keras.optimizers import Adam, SGD

adam = Adam(learning_rate=0.001, decay=1e-06)
# sgd = SGD(lr=0.01, momentum=0.9)
model.compile(loss='mse', optimizer=adam, metrics=['mae'])  # check in  tf.keras.losses and tf.keras.metrics
callbacks = [keras.callbacks.ModelCheckpoint(fileNameModel, save_best_only=True, monitor='mae', verbose=1, mode='min')]

print("Fit model on training data")

history = model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=numEpochs,
    # We pass some validation for
    # monitoring validation loss and metrics
    # at the end of each epoch
    validation_data=(x_val, y_val),
    callbacks=callbacks, 
    verbose=1
)

import numpy as np

# Evaluate using Training Data
y_train_preds = model.predict(x_train)
y_train_preds = np.argmax(y_train_preds, axis=1)

# Evaluate using Validation Data
y_val_preds = model.predict(x_val)
y_val_preds = np.argmax(y_val_preds, axis=1)

# Evaluate using Testing Data
y_test_preds = model.predict(x_test)
y_test_preds = np.argmax(y_test_preds, axis=1)

#saving data to file
from scipy.io import savemat

y_train = (np.argmax(y_train, axis=1))
y_val = (np.argmax(y_val, axis=1))
y_test = (np.argmax(y_test, axis=1))
# save to integer format
y_train.astype(int)
y_train_preds.astype(int)
y_val.astype(int)
y_val_preds.astype(int)
y_test.astype(int)
y_test_preds.astype(int)

matData = {"YTr": y_train, "YTrP": y_train_preds, "YV": y_val, "YVP": y_val_preds, "YTt": y_test, "YTtP": y_test_preds}
savemat(fileNameReport,matData)

import pandas as pd

# convert the history.history dict to a pandas DataFrame:     
hist_df = pd.DataFrame(history.history) 

# save history to csv: 
with open(fileNameHist, mode='w') as f:
    hist_df.to_csv(f)

print('Done')