% createDatasets

clear
close all
clc

% Parameter
windowSize = 8;

% Persentage Data of each class
persenDataUsed = [0.1; 1; 1; 1; 0.5; 1; 1];

% Modis Image
% band_1 = readgeoraster('11.tif'); %Band 1
% band_2 = readgeoraster('22.tif'); %Band 2
% band_3 = readgeoraster('33.tif'); %Band 3
% band_4 = readgeoraster('44.tif'); %Band 4
% band_5 = readgeoraster('55.tif'); %Band 5
% band_6 = readgeoraster('66.tif'); %Band 6
% band_7 = readgeoraster('77.tif'); %Band 7

% Modis Index
%rgri = readgeoraster('rgri.tif'); 
savi = readgeoraster('savi.tif'); 
evi = readgeoraster('evi.tif'); 
ndwi = readgeoraster('ndwi.tif');
grvi = readgeoraster('grvi.tif'); 
ndii = readgeoraster('ndii7.tif'); 
ndvi = readgeoraster('ndvi.tif'); 
ndfi = readgeoraster('ndfi.tif'); 
gndvi = readgeoraster('gndvi.tif'); 

%modis  = double(squeeze(cat(3,band_1,band_2,band_3,band_4,band_5,band_6,band_7)));
modis  = double(squeeze(cat(3,savi,evi,ndwi,grvi,ndii,ndvi,ndfi,gndvi)));

% Peat Land Class
peatLand = readgeoraster('peat.tif'); %Peatland

peatLand(peatLand==5) = 6;

% Counting peat Land distribution
[peatClass,~,idx] = unique(peatLand);
peatCount = accumarray(idx(:),1);

% Show peat Class Distribution in Bar Graph
figure;
bar(peatClass, peatCount)

feature = cell(length(peatClass),1);
target  = cell(length(peatClass),1);

for i=1:length(peatClass)
    disp(['Processing Class ' num2str(peatClass(i)) ]);
    [r,c]       = find(peatLand==peatClass(i));
    subModis    = zeros(windowSize,windowSize,size(modis,3),1);
    subPeatLand = zeros(1,1);
    idx = 1;
    halfWin = floor(windowSize*0.5);
    for j=1:length(r) 
        if (r(j)-halfWin > 0) && (r(j)+halfWin < size(modis,1)) && (c(j)-halfWin > 0) && (c(j)+halfWin < size(modis,2))
            subModis(:,:,:,idx) = modis(r(j)-halfWin:r(j)+halfWin-1,c(j)-halfWin:c(j)+halfWin-1,:);
            subPeatLand(1,idx)  = peatLand(r(j),c(j));
            idx = idx+1;
        end
    end
    
    % reduce the data to make more balance of each class
    newSize     = floor(persenDataUsed(i)*length(subModis));
    newIdx      = randperm(length(subModis), newSize);
    subModis    = subModis(:,:,:,newIdx);
    subPeatLand = subPeatLand(newIdx);

    feature{i} = permute(subModis,[4 1 2 3]);    
    target{i}  = permute(subPeatLand, [2 1]);
    clear subModis;
    clear subPeatLand;
end

% Show peat Class Distribution in Table
peatCountNow = zeros(length(feature),1);
for i = 1:length(feature)
    peatCountNow(i) = length(feature{i});
end

tablePeat = table(peatClass, peatCount, peatCountNow);
disp(tablePeat);

feature = cell2mat(feature);
target  = cell2mat(target);

feature = permute(feature,[4 3 2 1]);
target  = permute(target, [4 3 2 1]);

% saving data to hdf5
feature_dims   = size(feature);
target_dims  = size(target);
fileHDF5 = ['Datasets/selected_' num2str(windowSize) 'x' num2str(windowSize) '.h5'];

if exist(fileHDF5, 'file')==2
    delete(fileHDF5);
end

h5create(fileHDF5,'/feature', feature_dims, 'Datatype', 'double')
h5create(fileHDF5,'/target', target_dims, 'Datatype', 'double')

h5write(fileHDF5,'/feature', feature)
h5write(fileHDF5,'/target', target)

h5disp(fileHDF5)
